# lgg-python-client

This client is an example of what would need to be included in a Game's API service. 
It communicates with the Latency.GG API using the Client's API Keys to retrieve the metrics and generate idents on behalf of the game/probe.

## Command
```bash
user@machine:~$ poetry run backend --help
Usage: backend [OPTIONS]

Options:
  --client-api TEXT    The URL of the Client API (e.g.
                       https://client.latency.gg)
  --client-token TEXT  The Bearer token for the Client API
  --addr TEXT          The address for the backend to listen on
  --port INTEGER       The port for the backend to listen on
  --help               Show this message and exit.
```

### Example
```bash
user@machine:~$ poetry run backend --client-api https://client.latency.gg --client-token some_token
```