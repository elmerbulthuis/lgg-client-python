from collections import defaultdict
from functools import partial
from ipaddress import ip_address

import click
from flask import Flask, request, jsonify

from client_api import generate_ident, get_providers, get_metrics

app = Flask(__name__)


@click.command()
@click.option(
    "--client-api",
    envvar="CLIENT_API",
    prompt="Latency.GG Client API URL",
    help="The URL of the Client API (e.g. https://client.latency.gg)",
)
@click.option(
    "--client-token",
    envvar="CLIENT_TOKEN",
    prompt="Bearer token for the Client API",
    help="The Bearer token for the Client API",
)
@click.option(
    "--addr",
    envvar="addr",
    prompt="Address to listen on",
    help="The address for the backend to listen on",
    default="0.0.0.0",  # nosec
)
@click.option(
    "--port",
    envvar="port",
    prompt="Port to listen on",
    help="The port for the backend to listen on",
    default=5001,
)
def main(client_api: str, client_token: str, addr: str, port: int):
    app.client_api = client_api
    app.client_token = client_token
    app.run(host=addr, port=port)


metrics_storage = partial(defaultdict, dict)


@app.route("/metrics", methods=["GET"])
def get_metrics_handler():
    ip = ip_address(request.remote_addr)
    ident = generate_ident(app.client_api, ip.compressed, app.client_token)
    providers = get_providers(app.client_api, app.client_token)
    clean_metrics = metrics_storage()
    stale_metrics = metrics_storage()
    for provider in providers:
        for location in provider["locations"]:
            metrics = get_metrics(
                app.client_api,
                app.client_token,
                provider["name"],
                location,
                request.remote_addr,
            )
            if metrics["stale"]:
                stale_metrics[provider["name"]][location] = metrics
            else:
                clean_metrics[provider["name"]][location] = metrics
    return (
        jsonify(
            source={"addr": ip.compressed, "version": ip.version, "ident": ident},
            clean_metrics=clean_metrics,
            stale_metrics=stale_metrics,
        ),
        200,
    )


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
